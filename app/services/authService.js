'use strict';
app.factory('authService', ['$http', '$q', '$filter' ,'localStorageService', function ($http, $q, $filter, localStorageService) {
 
    var date = new Date();
    var fechahoy = $filter('date')(date, "yyyy-MM-dd");

    var serviceBase = 'http://ngauthenticationapi.azurewebsites.net/';
    var authServiceFactory = {};
 
    var _authentication = {
        isAuth: false,
        userName : "",
        nombre: "",
        puesto: "",
        seguridad: ""
    };
 
    var _saveRegistration = function (registration) {
 
        _logOut();
 
        return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
            return response;
        });
 
    };
 
    var _login = function (loginData) {
 
        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
 
        var deferred = $q.defer();
 
        $http.post('modelv2/v2logintoken.php',{ usuario : loginData.userName , contrasena : loginData.password })

        .then(function onSuccess(response) {

            if(response.data.usuarioinfo == false){
                 _logOut();
                 deferred.reject("El usuario o contraseña son incorrectos.");
            }
            else{
                localStorageService.set('authorizationData', { token: response.data.access_token, userName: loginData.userName, 
                userinfo: response.data.usuarioinfo, FechaIngreso : fechahoy });
 
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
            _authentication.nombre = response.data.usuarioinfo.nombre;
            _authentication.puesto = response.data.usuarioinfo.puesto;
            _authentication.seguridad = response.data.usuarioinfo.seguridad;
 
            deferred.resolve(response);
            }
            
 
        }, function onError(response){
            _logOut();
            deferred.reject(err);
        });
 
        return deferred.promise;
 
    };
 
    var _logOut = function () {
 
        localStorageService.remove('authorizationData');
 
        _authentication.isAuth = false;
        _authentication.userName = "";
 
    };
 
    var _fillAuthData = function () {
 
        var authData = localStorageService.get('authorizationData');
        if (authData)
        {
            if(authData.FechaIngreso < fechahoy){
                _logOut();
            }
            else{
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            _authentication.nombre = authData.userinfo.nombre;
            _authentication.puesto = authData.userinfo.puesto;
            _authentication.seguridad = authData.userinfo.seguridad;
             } 
        }
 
    }
 
    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
 
    return authServiceFactory;
}]);