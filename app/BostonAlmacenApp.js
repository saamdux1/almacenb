(function(){

    var firebaseConfig = {
              apiKey: "AIzaSyCWhhtIva5CYMdHFLHpLoauxV7lK4VP6OI",
              authDomain: "boston-8a3d1.firebaseapp.com",
              databaseURL: "https://boston-8a3d1.firebaseio.com",
              projectId: "boston-8a3d1",
              storageBucket: "boston-8a3d1.appspot.com",
              messagingSenderId: "377792531733",
              appId: "1:377792531733:web:888189b9f52877c1"
            };
            // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
}());


 var app = angular.module('appBoston', ['ngRoute', 'ngAnimate', 'ui', 'LocalStorageModule', 'ui.bootstrap', 'ngTable', 'toaster', 'ngLoadingSpinner','firebase','angular-barcode','ngFileUpload','angular.filter']);

 app.constant('bostonbd', 'boston_almacen_piloto');
 //app.constant('bostonbd', 'boston_almacen');

    app.config(['$routeProvider', '$httpProvider', '$locationProvider',
        function ($routeProvider, $httpProvider, $locationProvider) {
            $routeProvider

            .when("/menuprincipal", { templateUrl: "views/bienvenido.html" })  

            $routeProvider.otherwise({ redirectTo: "menuprincipal" });
        }]);



    //Directiva para el modal
    app.directive('modal', function () {
        return {
            template: '<div class="modal fade">' +
                '<div class="modal-dialog modal-lg">' +
                  '<div class="modal-content">' +
                    '<div class="modal-header">' +
                      '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                      '<h4 class="modal-title">{{ title }}</h4>' +
                    '</div>' +
                    '<div class="modal-body" ng-transclude></div>' +
                  '</div>' +
                '</div>' +
              '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title = attrs.title;

                scope.$watch(attrs.visible, function (value) {
                    if (value == true) {

                        $(element).modal('show');
                    }
                    else {
                        $(element).modal('hide');
                    }
                });

                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });

                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });

    //Factory para exportar a excel
    app.factory('Excel', function ($window) {
        var uri = 'data:application/vnd.ms-excel;base64,',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/html401"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64 = function (s) { return $window.btoa(unescape(encodeURIComponent(s))); },
            format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
        return {
            tableToExcel: function (tableId, worksheetName) {
                var table = $(tableId),
                    ctx = { worksheet: worksheetName, table: table.html() },
                    href = uri + base64(format(template, ctx));
                return href;
            }
        };
    });


    app.service('inventario', function() {
        this.myFunc = function (x) {
            var pruebalist=[
                {nombre:'Nombre1', apellido:'Apellido1'},
                {nombre:'Nombre2', apellido:'Apellido2'}
            ];
            return pruebalist;
        }
    });