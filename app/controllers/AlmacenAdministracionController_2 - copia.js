app.controller('AlmacenAdministracionCtrl',['$scope','$location','$http','$filter','toaster','NgTableParams', '$window', 'Excel', '$firebaseObject', '$firebaseArray', 'bostonbd', '$rootScope', '$timeout', 'inventario', 'localStorageService',
	function($scope,$location,$http,$filter,toaster, NgTableParams, $window, Excel, $firebaseObject, $firebaseArray, bostonbd, $rootScope, $timeout, inventario, localStorageService){

		var BDBoston = firebase.database().ref().child(bostonbd);

		var tableproveedores = BDBoston.child('proveedores');
		var tablefamiliadisenios = BDBoston.child('familiadisenios');
		var tablefamiliacolores = BDBoston.child('familiacolores');
		var tableclientes = BDBoston.child('clientes');
		var tablealmacenproductoterminado = BDBoston.child('almacenproductoterminado');
		var tablealmacenproductoterminadoentradas = BDBoston.child('almacenproductoterminadoentradas');
		var tablealmacenproductoterminadosalidas = BDBoston.child('almacenproductoterminadosalidas');
		var tablefamiliatipoprenda = BDBoston.child('familiatipoprenda');
		var tablefamiliarutas = BDBoston.child('familiarutas');
		var tablefamiliaplazasrutas = BDBoston.child('familiaplazasrutas');
		var tablefoliosfamilias = BDBoston.child('foliosfamilias');
		var tablefolioscolores = BDBoston.child('folioscolores');

		$scope.tableproveedoreslist = $firebaseArray(tableproveedores);
		$scope.tablefamiliadisenioslist = $firebaseArray(tablefamiliadisenios);
		$scope.tablefamiliacoloreslist = $firebaseArray(tablefamiliacolores);
		$scope.tableclienteslist = $firebaseArray(tableclientes);
		$scope.tablealmacenproductoterminadolist = $firebaseArray(tablealmacenproductoterminado);
		$scope.tablealmacenproductoterminadoentradaslist = $firebaseArray(tablealmacenproductoterminadoentradas);
		$scope.tablealmacenproductoterminadosalidaslist = $firebaseArray(tablealmacenproductoterminadosalidas);
		$scope.tablefamiliatipoprendalist = $firebaseArray(tablefamiliatipoprenda);
		$scope.tablefamiliarutaslist = $firebaseArray(tablefamiliarutas);
		$scope.tablefamiliaplazasrutaslist = $firebaseArray(tablefamiliaplazasrutas);
		$scope.tablefoliosfamiliaslist = $firebaseArray(tablefoliosfamilias);
		$scope.tablefolioscoloreslist = $firebaseArray(tablefolioscolores);

		$scope.tableproveedoreslist.$loaded()
	      	.then(function() {})
	      	.catch(function(err) {
	        	console.error(err);
	    });


	    $scope.tablealmacenproductoterminadolist.$loaded()
	      	.then(function() {
	      		$scope.CopiaAlmacenProductoTerminado = angular.copy($scope.tablealmacenproductoterminadolist);
	      	})
	      	.catch(function(err) {
	        	console.error(err);
	    });

	     

		$scope.movimiento = '';
		$scope.ListaEntradas=[];
		$scope.ListaSalidas=[];
		$scope.CorteSeleccionado = '';
		$scope.TotaldeTotales = 0;
		$scope.TotaldeTotalesSalida = 0;
		$scope.TotaldeTotalesInventario = 0;
		$scope.TotalCorte = 0;
		$scope.entrada={
			folio:'',
			fecha:''
		};
		$scope.BanderaTotales = 0;
		$scope.FamiliaSeleccionada = '';
		document.getElementById("salidacodigobarras").focus();

		$scope.TipoEntrada = 'PRODUCTO TERMINADO';
		$scope.TipoSalida = 'CARGA RUTAS';

		function AgregaCorteNuevo (){ 
			$scope.ListaEntradas.push({
				numerocorte:'',
				modelo:'',
				color:'',
				sexo:'',
				tipoprenda:'',
				proveedorclienteruta:'',
				tipoentrada:'',
				folio: '',
				fechaentrada: '',
				fecharegistro: '',
				tallasa:{talla:'', cantidad:0},
				tallasb:{talla:'', cantidad:0},
				tallasc:{talla:'', cantidad:0},
				tallasd:{talla:'', cantidad:0},
				tallase:{talla:'', cantidad:0},
				tallasf:{talla:'', cantidad:0},
				tallasg:{talla:'', cantidad:0},
				tallash:{talla:'', cantidad:0},
				tallasi:{talla:'', cantidad:0},
				tallasj:{talla:'', cantidad:0},
				tallask:{talla:'', cantidad:0},
				tallasl:{talla:'', cantidad:0},
				tallasm:{talla:'', cantidad:0},
				tallasn:{talla:'', cantidad:0},
				totalpiezas:0
			});
		}

		AgregaCorteNuevo();


		$scope.CargaAlmacen = function(tipomovimiento){
			$scope.movimiento = tipomovimiento;
			if(tipomovimiento=='Inventario')
				TotalDeTotalesInventario();
		};


		function ActualizaTotaldeTotales(){
			$scope.TotaldeTotales = 0;
			angular.forEach($scope.ListaEntradas, function(value, key) {
				$scope.TotaldeTotales += value.tallasa.cantidad*1;
				$scope.TotaldeTotales += value.tallasb.cantidad*1;
				$scope.TotaldeTotales += value.tallasc.cantidad*1;
				$scope.TotaldeTotales += value.tallasd.cantidad*1;
				$scope.TotaldeTotales += value.tallase.cantidad*1;
				$scope.TotaldeTotales += value.tallasf.cantidad*1;
				$scope.TotaldeTotales += value.tallasg.cantidad*1;
				$scope.TotaldeTotales += value.tallash.cantidad*1;
				$scope.TotaldeTotales += value.tallasi.cantidad*1;
				$scope.TotaldeTotales += value.tallasj.cantidad*1;
				$scope.TotaldeTotales += value.tallask.cantidad*1;
				$scope.TotaldeTotales += value.tallasl.cantidad*1;
				$scope.TotaldeTotales += value.tallasm.cantidad*1;	
				$scope.TotaldeTotales += value.tallasn.cantidad*1;
			});
		};


		function ActualizaTotalCorte(corte){
			corte.totalpiezas = 0;
			corte.totalpiezas += corte.tallasa.cantidad*1;
			corte.totalpiezas += corte.tallasb.cantidad*1;
			corte.totalpiezas += corte.tallasc.cantidad*1;
			corte.totalpiezas += corte.tallasd.cantidad*1;
			corte.totalpiezas += corte.tallase.cantidad*1;
			corte.totalpiezas += corte.tallasf.cantidad*1;
			corte.totalpiezas += corte.tallasg.cantidad*1;
			corte.totalpiezas += corte.tallash.cantidad*1;
			corte.totalpiezas += corte.tallasi.cantidad*1;
			corte.totalpiezas += corte.tallasj.cantidad*1;
			corte.totalpiezas += corte.tallask.cantidad*1;
			corte.totalpiezas += corte.tallasl.cantidad*1;
			corte.totalpiezas += corte.tallasm.cantidad*1;
			corte.totalpiezas += corte.tallasn.cantidad*1;
		};


		function ActualizaTotaldeTotalesSalidas(){
			$scope.TotaldeTotalesSalida = 0;
			angular.forEach($scope.ListaSalidas, function(value, key) {
				$scope.TotaldeTotalesSalida += value.tallasa.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasb.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasc.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasd.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallase.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasf.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasg.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallash.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasi.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasj.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallask.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasl.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasm.cantidadsalida*1;
				$scope.TotaldeTotalesSalida += value.tallasn.cantidadsalida*1;		
			});
		}

		function ActualizaTotalCorteSalida(corte){
			corte.totalpiezassalida = 0;
			corte.totalpiezassalida += corte.tallasa.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasb.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasc.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasd.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallase.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasf.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasg.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallash.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasi.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasj.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallask.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasl.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasm.cantidadsalida*1;
			corte.totalpiezassalida += corte.tallasn.cantidadsalida*1;
		};



		function cargafolios(){

	      	$scope.tablefoliosfamiliaslist.$loaded()	      		
		      	.then(function() {
		      		function padLeadingZeros(num, size) {
					    s = num+"";
					    while (s.length < size) s = "0" + s;
					    return s;
					}

					var s = '';
					$scope.incio = "F";
		      		$scope.nuevocodigo = "";
			        $scope.ultimocodigo = $scope.tablefoliosfamiliaslist.length - 1;
			        if($scope.ultimocodigo < 0)
			            $scope.nuevocodigo = "F001";
			        else{
			            //$scope.nuevocodigo = $scope.foliosentradaslist[ultimocodigo].codigo + 1;
			            var res = $scope.tablefoliosfamiliaslist[$scope.tablefoliosfamiliaslist.length - 1].folio.split("F");
			            padLeadingZeros((res[1]*1) + 1, 3); //"0057"
			            $scope.nuevocodigo = $scope.incio + s;
			            //$scope.entrada.folio = $scope.incio + s;
			        }			        
			        $scope.modeloaguardarfolio={
			            folio : $scope.nuevocodigo
			        };	
	
					
		      	})
		      	.catch(function(err) {
		        	console.error(err);
		    });

	    };

	    function cargafolioscolores(){

	      	$scope.tablefolioscoloreslist.$loaded()	      		
		      	.then(function() {
		      		function padLeadingZeros(num, size) {
					    s = num+"";
					    while (s.length < size) s = "0" + s;
					    return s;
					}

					var s = '';
					$scope.incio = "C";
		      		$scope.nuevocodigocolor = "";
			        $scope.ultimocodigo = $scope.tablefolioscoloreslist.length - 1;
			        if($scope.ultimocodigo < 0)
			            $scope.nuevocodigocolor = "C001";
			        else{
			            //$scope.nuevocodigo = $scope.foliosentradaslist[ultimocodigo].codigo + 1;
			            var res = $scope.tablefolioscoloreslist[$scope.tablefolioscoloreslist.length - 1].folio.split("C");
			            padLeadingZeros((res[1]*1) + 1, 3); //"0057"
			            $scope.nuevocodigocolor = $scope.incio + s;
			            //$scope.entrada.folio = $scope.incio + s;
			        }			        
			        $scope.modeloaguardarfoliocolor={
			            folio : $scope.nuevocodigocolor
			        };	
	
					
		      	})
		      	.catch(function(err) {
		        	console.error(err);
		    });

	    };


	    function TotalDeTotalesInventario(){
	    	$scope.TotaldeTotalesInventario = 0;
      		angular.forEach($scope.tablealmacenproductoterminadolist, function(value, key) {
      			$scope.TotaldeTotalesInventario += value.totalpiezas;
      		});
	    };

	    
	    cargafolios();
	    cargafolioscolores();


		$scope.AgregarCorte = function(){
			AgregaCorteNuevo();
			
		};

		$scope.EliminarCorteLista = function(corte, index){
			$scope.ListaEntradas.splice(index,1);
		};


		$scope.ActualizaTotales= function(corteseleccionado){
			ActualizaTotaldeTotales();
			ActualizaTotalCorte(corteseleccionado);
		};


		$scope.GuardaEntradaAlmacen = function(){
			if($scope.entrada.fecha == '' || $scope.entrada.fecha == null || $scope.entrada.fecha == undefined){
				toaster.pop('warning','AVISO','La fecha no puede estar vacía');
			}
			else if($scope.entrada.cveproveedor == '' || $scope.entrada.cveproveedor == null || $scope.entrada.cveproveedor == undefined){
					toaster.pop('warning','AVISO','El Proveedor-Cliente no puede estar vacío');
				}
				else if($scope.entrada.folio == '' || $scope.entrada.folio == null || $scope.entrada.folio == undefined){
					toaster.pop('warning','AVISO','El Folio no puede estar vacío');
				}
				else{
					var tamaniototal = 0;
					angular.forEach($scope.ListaEntradas, function(valueaguardar, key) {
						tamaniototal +=1;
						valueaguardar.tallasa.cantidad = Number(valueaguardar.tallasa.cantidad);
						valueaguardar.tallasb.cantidad = Number(valueaguardar.tallasb.cantidad);
						valueaguardar.tallasc.cantidad = Number(valueaguardar.tallasc.cantidad);
						valueaguardar.tallasd.cantidad = Number(valueaguardar.tallasd.cantidad);
						valueaguardar.tallase.cantidad = Number(valueaguardar.tallase.cantidad);
						valueaguardar.tallasf.cantidad = Number(valueaguardar.tallasf.cantidad);
						valueaguardar.tallasg.cantidad = Number(valueaguardar.tallasg.cantidad);
						valueaguardar.tallash.cantidad = Number(valueaguardar.tallash.cantidad);
						valueaguardar.tallasi.cantidad = Number(valueaguardar.tallasi.cantidad);
						valueaguardar.tallasj.cantidad = Number(valueaguardar.tallasj.cantidad);
						valueaguardar.tallask.cantidad = Number(valueaguardar.tallask.cantidad);
						valueaguardar.tallasl.cantidad = Number(valueaguardar.tallasl.cantidad);
						valueaguardar.tallasm.cantidad = Number(valueaguardar.tallasm.cantidad);
						valueaguardar.tallasn.cantidad = Number(valueaguardar.tallasm.cantidad);
						GuardaUnoPorUno(valueaguardar);

						if(tamaniototal == $scope.ListaEntradas.length){
							$timeout( function(){
					            $scope.ListaEntradas=[];
								$scope.TotaldeTotales = 0;
								$scope.TotalCorte = 0;
								$scope.entrada={
									folio:'',
									fecha:'',
									cveproveedor:''
								};
					        }, 2000 );							
						}
					});	
				}	
		};


		function GuardaNuevoCorte(corteaguardar){
			$scope.tablealmacenproductoterminadolist.$add(corteaguardar)
	            .then(function() {                          
	                toaster.pop('success','Operación Exitosa','La entrada se registró correctamente');
	            }).catch(function(error) {
	                toaster.pop('error','Operación Errónea','La entrada no se pudo registrar : ' + error);
	            });   		
		};

		function ActualizaCorte(infocortedebd,infocorteactualizar){
			infocortedebd.tallasa.cantidad = infocortedebd.tallasa.cantidad + infocorteactualizar.tallasa.cantidad;
			infocortedebd.tallasb.cantidad = infocortedebd.tallasb.cantidad + infocorteactualizar.tallasb.cantidad;
			infocortedebd.tallasc.cantidad = infocortedebd.tallasc.cantidad + infocorteactualizar.tallasc.cantidad;
			infocortedebd.tallasd.cantidad = infocortedebd.tallasd.cantidad + infocorteactualizar.tallasd.cantidad;
			infocortedebd.tallase.cantidad = infocortedebd.tallase.cantidad + infocorteactualizar.tallase.cantidad;
			infocortedebd.tallasf.cantidad = infocortedebd.tallasf.cantidad + infocorteactualizar.tallasf.cantidad;
			infocortedebd.tallasg.cantidad = infocortedebd.tallasg.cantidad + infocorteactualizar.tallasg.cantidad;
			infocortedebd.tallash.cantidad = infocortedebd.tallash.cantidad + infocorteactualizar.tallash.cantidad;
			infocortedebd.tallasi.cantidad = infocortedebd.tallasi.cantidad + infocorteactualizar.tallasi.cantidad;
			infocortedebd.tallasj.cantidad = infocortedebd.tallasj.cantidad + infocorteactualizar.tallasj.cantidad;
			infocortedebd.tallask.cantidad = infocortedebd.tallask.cantidad + infocorteactualizar.tallask.cantidad;
			infocortedebd.tallasl.cantidad = infocortedebd.tallasl.cantidad + infocorteactualizar.tallasl.cantidad;
			infocortedebd.tallasm.cantidad = infocortedebd.tallasm.cantidad + infocorteactualizar.tallasm.cantidad;
			infocortedebd.tallasn.cantidad = infocortedebd.tallasn.cantidad + infocorteactualizar.tallasn.cantidad;
			infocortedebd.totalpiezas = infocortedebd.tallasa.cantidad+infocortedebd.tallasb.cantidad+infocortedebd.tallasc.cantidad+infocortedebd.tallasd.cantidad+
										infocortedebd.tallase.cantidad+infocortedebd.tallasf.cantidad+infocortedebd.tallasg.cantidad+infocortedebd.tallash.cantidad+
										infocortedebd.tallasi.cantidad+infocortedebd.tallasj.cantidad+infocortedebd.tallask.cantidad+infocortedebd.tallasl.cantidad+
										infocortedebd.tallasm.cantidad+infocortedebd.tallasn.cantidad;

			$scope.tablealmacenproductoterminadolist.$save(infocortedebd).then(function() {
                toaster.pop('success','Operación Exitosa','El almacén se actualizó correctamente.');                                    
                }).catch(function(error) {
                    toaster.pop('error','Operación Errónea','El almacén no se pudo actualizar : ' + error);
            });

		};


		function GuardaEnAlmacenProductoTerminadoEntradas(corteaguardar){
			$scope.tablealmacenproductoterminadoentradaslist.$add(corteaguardar)
	            .then(function() {                          
	                toaster.pop('info','Operación Exitosa','');
	            }).catch(function(error) {
	                toaster.pop('error','Operación Errónea','La entrada no se pudo registrar : ' + error);
	            });   			
		};


		function GuardaUnoPorUno(corteaguardar){
			var bandera = 0;
			var tamanio = 0;
			var modeloaguardar = JSON.parse(corteaguardar.modelo);
			var coloraguardar = JSON.parse(corteaguardar.color);

			$scope.dateregistro = new Date();
			corteaguardar.folio = $scope.entrada.folio;
			corteaguardar.fechaentrada = $filter('date')($scope.entrada.fecha, "yyyy-MM-dd");
			corteaguardar.fecharegistro = $filter('date')($scope.dateregistro, "yyyy-MM-dd HH:mm:ss");
			corteaguardar.proveedorclienteruta = $scope.entrada.cveproveedor;
			corteaguardar.tipoentrada = $scope.TipoEntrada;
			corteaguardar.tipomovimiento = 'ENTRADA';
			corteaguardar.modelo = modeloaguardar.familia;
			corteaguardar.foliofamilia = modeloaguardar.folio;
			corteaguardar.foliocolor = coloraguardar.folio;
			corteaguardar.color = coloraguardar.color;

			if($scope.tablealmacenproductoterminadolist.length==0){
				GuardaNuevoCorte(corteaguardar);
				GuardaEnAlmacenProductoTerminadoEntradas(corteaguardar);
			}
			else{
				angular.forEach($scope.tablealmacenproductoterminadolist, function(listadecortesbd, key) {
					if(listadecortesbd.modelo == corteaguardar.modelo && listadecortesbd.color == corteaguardar.color){
						bandera = 1;
						ActualizaCorte(listadecortesbd,corteaguardar);
						GuardaEnAlmacenProductoTerminadoEntradas(corteaguardar);
					}
					tamanio += 1;

					if(tamanio == $scope.tablealmacenproductoterminadolist.length && bandera==0){
						GuardaNuevoCorte(corteaguardar);
						GuardaEnAlmacenProductoTerminadoEntradas(corteaguardar);
					}
				});	
			}			
		}



		// Código para Agregar un NUEVO PROVEEDOR //
		$scope.ProveedorSeleccionado = function(proveedor){
			if(proveedor == 'AGREGAR...'){
				$('#AgregaProveedorModal').modal('show');
				$scope.entrada.cveproveedor= '';
				$scope.proveedor.empresa= '';
				$scope.proveedor.notas= '';
			}
		};

		$scope.GuardarProveedor = function(proveedor){
			proveedor.empresa = proveedor.empresa.toUpperCase();
			proveedor.notas = proveedor.notas.toUpperCase();
			$scope.tableproveedoreslist.$add(proveedor)
	            .then(function() {                          
	                toaster.pop('success','Operación Exitosa','El proveedor se registró correctamente');
	                $('#AgregaProveedorModal').modal('hide');
	            }).catch(function(error) {
	                toaster.pop('error','Operación Errónea','El proveedor no se pudo registrar : ' + error);
	            });   		
		};
		//////////////////////////////////////////////



		// Códiugo para Agregar un NUEVO CLIENTE //
		$scope.ClienteSeleccionado = function(cliente){
			if(cliente == 'AGREGAR...'){
				$('#AgregaClienteModal').modal('show');
				$scope.entrada.cveproveedor= '';
				$scope.cliente.nombre= '';
				$scope.cliente.notas= '';
			}
		};


		$scope.GuardarCliente = function(cliente){
			cliente.nombre = cliente.nombre.toUpperCase();
			cliente.notas = cliente.notas.toUpperCase();
			$scope.tableclienteslist.$add(cliente)
	            .then(function() {                          
	                toaster.pop('success','Operación Exitosa','El cliente se registró correctamente');
	                $('#AgregaClienteModal').modal('hide');
	            }).catch(function(error) {
	                toaster.pop('error','Operación Errónea','El cliente no se pudo registrar : ' + error);
	            });   		
		};
		///////////////////////////////////////////////////



		// Código para Agregar un NUEVO MODELO-DISEÑO //
		$scope.ModeloSeleccionado = function(modelo,index){
			$scope.indexAgregaModelo = index;

			if(modelo == 'AGREGAR...'){
				$('#AgregaModeloModal').modal('show');
                $scope.ListaEntradas[$scope.indexAgregaModelo].modelo = '';
				$scope.modelo.familia= '';
				$scope.modelo.sexo= '';
				$scope.modelo.tipotalla= '';
				cargafolios();
			}
			else{
				$scope.FamiliaSeleccionada = JSON.parse(modelo);
				$scope.ListaEntradas[$scope.indexAgregaModelo].color = '';
				$scope.ListaEntradas[$scope.indexAgregaModelo].sexo = $scope.FamiliaSeleccionada.sexo;
				//$scope.ListaEntradas[$scope.indexAgregaModelo].modelo = $scope.FamiliaSeleccionada.familia;
				$scope.ListaEntradas[$scope.indexAgregaModelo].tipotalla = $scope.FamiliaSeleccionada.tipotalla;
				switch ($scope.FamiliaSeleccionada.tipotalla) {
		            case 'TALLAS HOMBRE':
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasa.talla = 28;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasb.talla = 30;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasc.talla = 32;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasd.talla = 34;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallase.talla = 36;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasf.talla = 38;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasg.talla = 40;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallash.talla = 42;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasi.talla = 44;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasj.talla = 46;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallask.talla = 48;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasl.talla = 50;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasm.talla = 52;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasn.talla = 'VAR';
		                break;
		            case 'TALLAS MUJER':
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasa.talla = 3;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasb.talla = 5;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasc.talla = 7;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasd.talla = 9;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallase.talla = 11;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasf.talla = 13;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasg.talla = 15;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallash.talla = 17;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasi.talla = 19;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasj.talla = 21;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallask.talla = 23;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasl.talla = 25;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasm.talla = 27;
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasn.talla = 'VAR';
		                break;
		            case 'TALLAS VARIAS':
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasa.talla = 'XS';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasb.talla = 'X';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasc.talla = 'M';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasd.talla = 'G';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallase.talla = 'XL';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasf.talla = '2XL';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasg.talla = '3XL';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallash.talla = '4XL';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasi.talla = '5XL';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasj.talla = '6XL';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallask.talla = '7XL';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasl.talla = '8XL';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasm.talla = '9XL';
		                $scope.ListaEntradas[$scope.indexAgregaModelo].tallasn.talla = 'VAR';
		                break;
		            default:

		        }
			}
		};

		
		$scope.GuardarModelo = function(modelo){
			if(modelo.sexo=='' || modelo.sexo== null || modelo.sexo==undefined){
				toaster.pop('warning','AVISO','Debes seleccionar el sexo para poder continuar');
			}
			else if(modelo.tipotalla=='' || modelo.tipotalla== null || modelo.tipotalla==undefined){
				toaster.pop('warning','AVISO','Debes seleccionar el tipo de talla para poder continuar');
			}
			else{
				modelo.activo = 1;
				modelo.familia = modelo.familia.toUpperCase();
				modelo.folio = $scope.modeloaguardarfolio.folio;
				$scope.tablefamiliadisenioslist.$add(modelo)
		            .then(function() {                          
		                toaster.pop('success','Operación Exitosa','El modelo se registró correctamente');		                
		                $('#AgregaModeloModal').modal('hide');
		                $scope.tablefoliosfamiliaslist.$add($scope.modeloaguardarfolio)
						        		.then(function() {
							      		}).catch(function(error) {
							        		toaster.pop('error','Operación Errónea','Los folios de la familia no se pudieron actualizar : ' + error);
							      		});			
		            }).catch(function(error) {
		                toaster.pop('error','Operación Errónea','El modelo no se pudo registrar : ' + error);
		            });   
		    }		
		};
		////////////////////////////////////////////////


		// Código para agregar un NUEVO COLOR //
		$scope.ColorSeleccionado = function(color,index){
			if(color == 'AGREGAR...'){
				$('#AgregaColorModal').modal('show');
				$scope.color.color= '';
				$scope.color.familiadisenio= '';
				cargafolioscolores();
			}			
		};

		$scope.GuardarColor = function(color){
			color.activo = 1;
			color.color = color.color.toUpperCase();
			color.folio = $scope.modeloaguardarfoliocolor.folio;
			$scope.tablefamiliacoloreslist.$add(color)
	            .then(function() {                          
	                toaster.pop('success','Operación Exitosa','El color se registró correctamente');
	                $('#AgregaColorModal').modal('hide');
	                $scope.tablefolioscoloreslist.$add($scope.modeloaguardarfoliocolor)
						        		.then(function() {
							      		}).catch(function(error) {
							        		toaster.pop('error','Operación Errónea','Los folios del color no se pudieron actualizar : ' + error);
						        		});			
	            }).catch(function(error) {
	                toaster.pop('error','Operación Errónea','El color no se pudo registrar : ' + error);
	            });   		
		};
		////////////////////////////////////////


		// Código para agregar una RUTA NUEVA //
		$scope.RutaSeleccionada = function(ruta){
			if(ruta == 'AGREGAR...'){
				$('#AgregaRutaModal').modal('show');
				$scope.salida.clienteruta= '';
				$scope.ruta.nombre= '';
				$scope.ruta.notas= '';
			}
		};

		$scope.GuardarRuta = function(ruta){
			ruta.nombre = ruta.nombre.toUpperCase();
			ruta.notas = ruta.notas.toUpperCase();
			$scope.tablefamiliarutaslist.$add(ruta)
	            .then(function() {                          
	                toaster.pop('success','Operación Exitosa','La ruta se registró correctamente');
	                $('#AgregaRutaModal').modal('hide');
	            }).catch(function(error) {
	                toaster.pop('error','Operación Errónea','La ruta no se pudo registrar : ' + error);
	            });   		
		};
		////////////////////////////////////////


		// Código para agregar una PLAZA NUEVA //
		$scope.PlazaRutaSeleccionada = function(plazaruta){
			if(plazaruta == 'AGREGAR...'){
				$('#AgregaPlazaRutaModal').modal('show');
				$scope.salida.plazaruta= '';
				$scope.plazaruta.nombre= '';
				$scope.plazaruta.notas= '';
			}
		};

		$scope.GuardarPlazaRuta = function(plazaruta){
			plazaruta.nombre = plazaruta.nombre.toUpperCase();
			plazaruta.notas = plazaruta.notas.toUpperCase();
			$scope.tablefamiliaplazasrutaslist.$add(plazaruta)
	            .then(function() {                          
	                toaster.pop('success','Operación Exitosa','La plaza se registró correctamente');
	                $('#AgregaPlazaRutaModal').modal('hide');
	            }).catch(function(error) {
	                toaster.pop('error','Operación Errónea','La plaza no se pudo registrar : ' + error);
	            });   		
		};
		////////////////////////////////////////


		$scope.ModeloSeleccionadoSalida = function(modeloseleccionado){
			$scope.CorteSeleccionado = JSON.parse(modeloseleccionado);
			$scope.codigo.color = "";
		};


		$scope.RevisaTotales = function(cantidad, talla){
			cantidad = cantidad*1;

			switch (talla) {
	            case 'a':
	                if(cantidad>($scope.CorteSeleccionado.tallasa.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'b':
	                if(cantidad>($scope.CorteSeleccionado.tallasb.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'c':
	                if(cantidad>($scope.CorteSeleccionado.tallasc.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'd':
	                if(cantidad>($scope.CorteSeleccionado.tallasd.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'e':
	                if(cantidad>($scope.CorteSeleccionado.tallase.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'f':
	                if(cantidad>($scope.CorteSeleccionado.tallasf.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'g':
	                if(cantidad>($scope.CorteSeleccionado.tallasg.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'h':
	                if(cantidad>($scope.CorteSeleccionado.tallash.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'i':
	                if(cantidad>($scope.CorteSeleccionado.tallasi.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'j':
	                if(cantidad>($scope.CorteSeleccionado.tallasj.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'k':
	                if(cantidad>($scope.CorteSeleccionado.tallask.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'l':
	                if(cantidad>($scope.CorteSeleccionado.tallasl.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'm':
	                if(cantidad>($scope.CorteSeleccionado.tallasm.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            case 'n':
	                if(cantidad>($scope.CorteSeleccionado.tallasn.cantidad*1)){
	                	toaster.pop('warning','AVISO','La cantidad no puede ser mayor a la existencia');
	                }
	                break;
	            default:
	        }

		};

		$scope.modif={
			tallasa:{talla:'', cantidad:0},
					tallasb:{talla:'', cantidad:0},
					tallasc:{talla:'', cantidad:0},
					tallasd:{talla:'', cantidad:0},
					tallase:{talla:'', cantidad:0},
					tallasf:{talla:'', cantidad:0},
					tallasg:{talla:'', cantidad:0},
					tallash:{talla:'', cantidad:0},
					tallasi:{talla:'', cantidad:0},
					tallasj:{talla:'', cantidad:0},
					tallask:{talla:'', cantidad:0},
					tallasl:{talla:'', cantidad:0},
					tallasm:{talla:'', cantidad:0},
					tallasn:{talla:'', cantidad:0}	
		};

		$scope.AgregaAListaSalida = function(modif){

			//if(modif.tallasa==undefined || modif.tallasa.cantidad=='' || modif.tallasa.cantidad==null)
				


			if(modif==null || modif=='' || modif==undefined){
				toaster.pop('warning','AVISO','El número de corte no puede estar vacío.');
			}
			else if(modif.numerocorte==null || modif.numerocorte=='' || modif.numerocorte==undefined){
				toaster.pop('warning','AVISO','El número de corte no puede estar vacío.');
			}
			else if($scope.CorteSeleccionado=='' || $scope.CorteSeleccionado==null || $scope.CorteSeleccionado==undefined){
				toaster.pop('warning','AVISO','Debes elegir un modelo para poder continuar.');
			}
			else if((modif.tallasa.cantidad*1)>($scope.CorteSeleccionado.tallasa.cantidad*1)||
					(modif.tallasb.cantidad*1)>($scope.CorteSeleccionado.tallasb.cantidad*1)||
					(modif.tallasc.cantidad*1)>($scope.CorteSeleccionado.tallasc.cantidad*1)||
					(modif.tallasd.cantidad*1)>($scope.CorteSeleccionado.tallasd.cantidad*1)||
					(modif.tallase.cantidad*1)>($scope.CorteSeleccionado.tallase.cantidad*1)||
					(modif.tallasf.cantidad*1)>($scope.CorteSeleccionado.tallasf.cantidad*1)||
					(modif.tallasg.cantidad*1)>($scope.CorteSeleccionado.tallasg.cantidad*1)||
					(modif.tallash.cantidad*1)>($scope.CorteSeleccionado.tallash.cantidad*1)||
					(modif.tallasi.cantidad*1)>($scope.CorteSeleccionado.tallasi.cantidad*1)||
					(modif.tallasj.cantidad*1)>($scope.CorteSeleccionado.tallasj.cantidad*1)||
					(modif.tallask.cantidad*1)>($scope.CorteSeleccionado.tallask.cantidad*1)||
					(modif.tallasl.cantidad*1)>($scope.CorteSeleccionado.tallasl.cantidad*1)||
					(modif.tallasm.cantidad*1)>($scope.CorteSeleccionado.tallasm.cantidad*1)||
					(modif.tallasn.cantidad*1)>($scope.CorteSeleccionado.tallasn.cantidad*1)){
				toaster.pop('warning','AVISO','Revisa las cantidades, las cantidades a sacar no pueden ser mayores a la existencia');
			}
			else{
				$scope.ListaSalidas.push({
					idalmacenproductoterminadooriginal: $scope.CorteSeleccionado.$id,
					numerocorte:modif.numerocorte,
					modelo:$scope.CorteSeleccionado.modelo,
					color:$scope.CorteSeleccionado.color,
					sexo:$scope.CorteSeleccionado.sexo,
					tipoprenda:$scope.CorteSeleccionado.tipoprenda,
					clienteruta:'',
					plaza:'',
					tiposalida:'',
					folio: '',
					fechasalida: '',
					fecharegistro: '',
					tallasa:{talla:'', cantidad:0 + modif.tallasa.cantidad*1},
					tallasb:{talla:'', cantidad:0 + modif.tallasb.cantidad*1},
					tallasc:{talla:'', cantidad:0 + modif.tallasc.cantidad*1},
					tallasd:{talla:'', cantidad:0 + modif.tallasd.cantidad*1},
					tallase:{talla:'', cantidad:0 + modif.tallase.cantidad*1},
					tallasf:{talla:'', cantidad:0 + modif.tallasf.cantidad*1},
					tallasg:{talla:'', cantidad:0 + modif.tallasg.cantidad*1},
					tallash:{talla:'', cantidad:0 + modif.tallash.cantidad*1},
					tallasi:{talla:'', cantidad:0 + modif.tallasi.cantidad*1},
					tallasj:{talla:'', cantidad:0 + modif.tallasj.cantidad*1},
					tallask:{talla:'', cantidad:0 + modif.tallask.cantidad*1},
					tallasl:{talla:'', cantidad:0 + modif.tallasl.cantidad*1},
					tallasm:{talla:'', cantidad:0 + modif.tallasm.cantidad*1},
					tallasn:{talla:'', cantidad:0 + modif.tallasn.cantidad*1},
					totalpiezas:modif.tallasa.cantidad*1+modif.tallasb.cantidad*1+modif.tallasc.cantidad*1+modif.tallasd.cantidad*1+modif.tallase.cantidad*1+modif.tallasf.cantidad*1+
								modif.tallasg.cantidad*1+modif.tallash.cantidad*1+modif.tallasi.cantidad*1+modif.tallasj.cantidad*1+modif.tallask.cantidad*1+modif.tallasl.cantidad*1+
								modif.tallasm.cantidad*1+modif.tallasn.cantidad*1
				});	


				$scope.modif={
					tallasa:{talla:'', cantidad:0},
							tallasb:{talla:'', cantidad:0},
							tallasc:{talla:'', cantidad:0},
							tallasd:{talla:'', cantidad:0},
							tallase:{talla:'', cantidad:0},
							tallasf:{talla:'', cantidad:0},
							tallasg:{talla:'', cantidad:0},
							tallash:{talla:'', cantidad:0},
							tallasi:{talla:'', cantidad:0},
							tallasj:{talla:'', cantidad:0},
							tallask:{talla:'', cantidad:0},
							tallasl:{talla:'', cantidad:0},
							tallasm:{talla:'', cantidad:0},
							tallasn:{talla:'', cantidad:0}	
				};
				$scope.x.modelo='';
				$scope.CorteSeleccionado = '';
				ActualizaTotaldeTotalesSalidas();
			}
		};




		$scope.EliminarCorteListaSalida = function(row, index){

			$scope.ListaSalidas[index].tallasa.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasb.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasc.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasd.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallase.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasf.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasg.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallash.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasi.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasj.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallask.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasl.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasm.cantidadsalida = 0;
			$scope.ListaSalidas[index].tallasn.cantidadsalida = 0;
			$scope.ListaSalidas[index].totalpiezassalida = 0;


			$scope.ListaSalidas.splice(index,1);
			ActualizaTotaldeTotalesSalidas();

			document.getElementById("salidacodigobarras").focus();


		};


		function ActualizaCorteSalida(infocortesalidabd){
			var modeloaactualizar = $scope.tablealmacenproductoterminadolist.$getRecord(infocortesalidabd.$id);


			modeloaactualizar.tallasa.cantidad = modeloaactualizar.tallasa.cantidad - infocortesalidabd.tallasa.cantidadsalida*1;
			modeloaactualizar.tallasb.cantidad = modeloaactualizar.tallasb.cantidad - infocortesalidabd.tallasb.cantidadsalida*1;
			modeloaactualizar.tallasc.cantidad = modeloaactualizar.tallasc.cantidad - infocortesalidabd.tallasc.cantidadsalida*1;
			modeloaactualizar.tallasd.cantidad = modeloaactualizar.tallasd.cantidad - infocortesalidabd.tallasd.cantidadsalida*1;
			modeloaactualizar.tallase.cantidad = modeloaactualizar.tallase.cantidad - infocortesalidabd.tallase.cantidadsalida*1;
			modeloaactualizar.tallasf.cantidad = modeloaactualizar.tallasf.cantidad - infocortesalidabd.tallasf.cantidadsalida*1;
			modeloaactualizar.tallasg.cantidad = modeloaactualizar.tallasg.cantidad - infocortesalidabd.tallasg.cantidadsalida*1;
			modeloaactualizar.tallash.cantidad = modeloaactualizar.tallash.cantidad - infocortesalidabd.tallash.cantidadsalida*1;
			modeloaactualizar.tallasi.cantidad = modeloaactualizar.tallasi.cantidad - infocortesalidabd.tallasi.cantidadsalida*1;
			modeloaactualizar.tallasj.cantidad = modeloaactualizar.tallasj.cantidad - infocortesalidabd.tallasj.cantidadsalida*1;
			modeloaactualizar.tallask.cantidad = modeloaactualizar.tallask.cantidad - infocortesalidabd.tallask.cantidadsalida*1;
			modeloaactualizar.tallasl.cantidad = modeloaactualizar.tallasl.cantidad - infocortesalidabd.tallasl.cantidadsalida*1;
			modeloaactualizar.tallasm.cantidad = modeloaactualizar.tallasm.cantidad - infocortesalidabd.tallasm.cantidadsalida*1;
			modeloaactualizar.tallasn.cantidad = modeloaactualizar.tallasn.cantidad - infocortesalidabd.tallasn.cantidadsalida*1;
			modeloaactualizar.totalpiezas = modeloaactualizar.totalpiezas - infocortesalidabd.totalpiezassalida*1;

			$scope.tablealmacenproductoterminadolist.$save(modeloaactualizar).then(function() {
                toaster.pop('success','Operación Exitosa','El almacén se actualizó correctamente.');                                    
                }).catch(function(error) {
                    toaster.pop('error','Operación Errónea','El almacén no se pudo actualizar : ' + error);
            });

		};

		function GuardaUnoPorUnoSalida(corteaguardarsalida){
			if($scope.TipoEntrada=='CLIENTES'){
				$scope.salida.plazaruta='N/A';	
			}

			$scope.dateregistro = new Date();
			corteaguardarsalida.folio = $scope.salida.folio;
			corteaguardarsalida.fechasalida = $filter('date')($scope.salida.fecha, "yyyy-MM-dd");
			corteaguardarsalida.fecharegistro = $filter('date')($scope.dateregistro, "yyyy-MM-dd HH:mm:ss");
			corteaguardarsalida.proveedorclienteruta = $scope.salida.clienteruta;
			corteaguardarsalida.plaza = $scope.salida.plazaruta;
			corteaguardarsalida.tiposalida = $scope.TipoSalida;
			corteaguardarsalida.tipomovimiento = 'SALIDA';
          
			$scope.tablealmacenproductoterminadosalidaslist.$add(corteaguardarsalida)
	            .then(function() {   
	            	ActualizaCorteSalida(corteaguardarsalida);                       
	                toaster.pop('success','Operación Exitosa','La salida se registró correctamente');
	            }).catch(function(error) {
	                toaster.pop('error','Operación Errónea','La entrada no se pudo registrar : ' + error);
	            });   		
		};

		$scope.GuardaSalidaAlmacen = function(){
			if($scope.salida.fecha == '' || $scope.salida.fecha == null || $scope.salida.fecha == undefined){
				toaster.pop('warning','AVISO','La fecha no puede estar vacía');
			}
			else if($scope.TipoSalida=='CLIENTES' && ($scope.salida.clienteruta == '' || $scope.salida.clienteruta == null || $scope.salida.clienteruta == undefined)){
					toaster.pop('warning','AVISO','El cliente no puede estar vacío');	
			}
			else if($scope.TipoSalida=='CARGA RUTAS' && ($scope.salida.clienteruta == '' || $scope.salida.clienteruta == null || $scope.salida.clienteruta == undefined)){
					toaster.pop('warning','AVISO','La ruta no puede estar vacía');	
			}
			else if($scope.TipoSalida=='CARGA RUTAS' && ($scope.salida.plazaruta == '' || $scope.salida.plazaruta == null || $scope.salida.plazaruta == undefined)){
					toaster.pop('warning','AVISO','La plaza no puede estar vacía');	
			}
			else if($scope.salida.folio == '' || $scope.salida.folio == null || $scope.salida.folio == undefined){
				toaster.pop('warning','AVISO','El folio no puede estar vacío');
			}
			else if($scope.ListaSalidas.length == 0){
				toaster.pop('warning','AVISO','Debes agregar al menos un corte para poder continuar.');
			}
			else{

				var totalaguardarsalidas = 0;
				angular.forEach($scope.ListaSalidas, function(valueaguardarsalida, key) {
					GuardaUnoPorUnoSalida(valueaguardarsalida);
					totalaguardarsalidas += 1;
					if(totalaguardarsalidas == $scope.ListaSalidas.length){
		    			$scope.salida.fecha = '';
		    			$scope.salida.clienteruta = '';
		    			$scope.salida.plazaruta = '';
		    			$scope.salida.folio = '';
						$scope.ListaSalidas=[];
						$scope.TotaldeTotalesSalida = 0;
					}
				});	
			}		
		};



		// Función para escanear código de barras //
		$scope.CodigoEscaneado = function(codigobarras){

			var familiainicio = 0;
			var familiafin = 4;
			var colorinicio = 4;
			var colorfin = 8;
			var tallainicio = 8;
			var tallafin = 9;

			var foliofamiliascan = codigobarras.substring(familiainicio, familiafin);
			var foliocolorscan = codigobarras.substring(colorinicio, colorfin);
			var tallascan = codigobarras.substring(tallainicio, tallafin);


			function agregatallasescaneadas (almacenproductoterminadolista,agregaalista){
				switch (tallascan) {
		            case 'A':
		            	if(almacenproductoterminadolista.tallasa.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasa.cantidadsalida>=almacenproductoterminadolista.tallasa.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
							if(almacenproductoterminadolista.tallasa.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasa.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasa.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;			                
			            }
		                break;
		            case 'B':
		                if(almacenproductoterminadolista.tallasb.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasb.cantidadsalida>=almacenproductoterminadolista.tallasb.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasb.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasb.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasb.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
	                case 'C':
		                if(almacenproductoterminadolista.tallasc.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasc.cantidadsalida>=almacenproductoterminadolista.tallasc.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasc.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasc.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasc.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'D':
		                if(almacenproductoterminadolista.tallasd.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasd.cantidadsalida>=almacenproductoterminadolista.tallasd.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasd.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasd.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasd.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'E':
		                if(almacenproductoterminadolista.tallase.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallase.cantidadsalida>=almacenproductoterminadolista.tallase.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallase.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallase.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallase.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'F':
		                if(almacenproductoterminadolista.tallasf.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasf.cantidadsalida>=almacenproductoterminadolista.tallasf.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasf.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasf.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasf.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'G':
		                if(almacenproductoterminadolista.tallasg.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasg.cantidadsalida>=almacenproductoterminadolista.tallasg.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasg.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasg.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasg.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'H':
		                if(almacenproductoterminadolista.tallash.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallash.cantidadsalida>=almacenproductoterminadolista.tallash.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallash.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallash.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallash.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'I':
		                if(almacenproductoterminadolista.tallasi.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasi.cantidadsalida>=almacenproductoterminadolista.tallasi.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasi.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasi.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasi.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'J':
		                if(almacenproductoterminadolista.tallasj.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasj.cantidadsalida>=almacenproductoterminadolista.tallasj.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasj.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasj.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasj.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'K':
		                if(almacenproductoterminadolista.tallask.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallask.cantidadsalida>=almacenproductoterminadolista.tallask.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallask.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallask.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallask.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'L':
		                if(almacenproductoterminadolista.tallasl.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasl.cantidadsalida>=almacenproductoterminadolista.tallasl.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasl.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasl.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasl.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'M':
		                if(almacenproductoterminadolista.tallasm.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasm.cantidadsalida>=almacenproductoterminadolista.tallasm.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasm.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasm.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasm.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            case 'N':
		                if(almacenproductoterminadolista.tallasn.cantidad<=0){
							toaster.pop('warning','AVISO','No cuentas con existencias en esa talla');
						}
						else if(almacenproductoterminadolista.tallasn.cantidadsalida>=almacenproductoterminadolista.tallasn.cantidad){
							toaster.pop('warning','AVISO','No puedes sacar más piezas de las que hay en existencia');
						}
						else{
			                if(almacenproductoterminadolista.tallasn.cantidadsalida==undefined)
			                	almacenproductoterminadolista.tallasn.cantidadsalida = 1;
			                else
			                	almacenproductoterminadolista.tallasn.cantidadsalida += 1;

			                if(almacenproductoterminadolista.totalpiezassalida==undefined)
			                	almacenproductoterminadolista.totalpiezassalida = 1;
			                else
			                	almacenproductoterminadolista.totalpiezassalida += 1;

			                $scope.TotaldeTotalesSalida += 1;
			            }
		                break;
		            default:
		        }	

		        if(agregaalista==1){
		        	$scope.ListaSalidas.push(almacenproductoterminadolista);
		        }

		        $scope.cortecodigobarras = '';
                document.getElementById("salidacodigobarras").focus();		
			}

			if($scope.ListaSalidas.length == 0){
				angular.forEach($scope.CopiaAlmacenProductoTerminado, function(almacenproductoterminadolista, key) {
					if(almacenproductoterminadolista.foliofamilia==foliofamiliascan && almacenproductoterminadolista.foliocolor==foliocolorscan){
						almacenproductoterminadolista.tallasa.cantidadsalida = 0;
						almacenproductoterminadolista.tallasb.cantidadsalida = 0;
						almacenproductoterminadolista.tallasc.cantidadsalida = 0;
						almacenproductoterminadolista.tallasd.cantidadsalida = 0;
						almacenproductoterminadolista.tallase.cantidadsalida = 0;
						almacenproductoterminadolista.tallasf.cantidadsalida = 0;
						almacenproductoterminadolista.tallasg.cantidadsalida = 0;
						almacenproductoterminadolista.tallash.cantidadsalida = 0;
						almacenproductoterminadolista.tallasi.cantidadsalida = 0;
						almacenproductoterminadolista.tallasj.cantidadsalida = 0;
						almacenproductoterminadolista.tallask.cantidadsalida = 0;
						almacenproductoterminadolista.tallasl.cantidadsalida = 0;
						almacenproductoterminadolista.tallasm.cantidadsalida = 0;
						almacenproductoterminadolista.tallasn.cantidadsalida = 0;
						almacenproductoterminadolista.totalpiezassalida = 0;
						agregatallasescaneadas(almacenproductoterminadolista,1);	
					} 
				});
			}
			else{
				var tamaniolistaaguardar = 0;
				var banderalistaaguardar = 0;
				angular.forEach($scope.ListaSalidas, function(valuelistasalidas, key) {
					if(valuelistasalidas.foliofamilia==foliofamiliascan && valuelistasalidas.foliocolor==foliocolorscan){
						agregatallasescaneadas(valuelistasalidas,0);
						banderalistaaguardar = 1;
					}  
					tamaniolistaaguardar += 1;
					if(tamaniolistaaguardar==$scope.ListaSalidas.length && banderalistaaguardar==0){
						angular.forEach($scope.CopiaAlmacenProductoTerminado, function(almacenproductoterminadolista, key) {
							if(almacenproductoterminadolista.foliofamilia==foliofamiliascan && almacenproductoterminadolista.foliocolor==foliocolorscan){
								almacenproductoterminadolista.tallasa.cantidadsalida = 0;
								almacenproductoterminadolista.tallasb.cantidadsalida = 0;
								almacenproductoterminadolista.tallasc.cantidadsalida = 0;
								almacenproductoterminadolista.tallasd.cantidadsalida = 0;
								almacenproductoterminadolista.tallase.cantidadsalida = 0;
								almacenproductoterminadolista.tallasf.cantidadsalida = 0;
								almacenproductoterminadolista.tallasg.cantidadsalida = 0;
								almacenproductoterminadolista.tallash.cantidadsalida = 0;
								almacenproductoterminadolista.tallasi.cantidadsalida = 0;
								almacenproductoterminadolista.tallasj.cantidadsalida = 0;
								almacenproductoterminadolista.tallask.cantidadsalida = 0;
								almacenproductoterminadolista.tallasl.cantidadsalida = 0;
								almacenproductoterminadolista.tallasm.cantidadsalida = 0;
								almacenproductoterminadolista.tallasn.cantidadsalida = 0;
								almacenproductoterminadolista.totalpiezassalida = 0;
								agregatallasescaneadas(almacenproductoterminadolista,1);	
							} 
						});	
					}
				});	
			}
		};





		$scope.imprimir = function(){
			// var divToPrint=document.getElementById("imprimirtabla");
	  //       newWin= window.open("");
	  //       newWin.document.write(divToPrint.outerHTML);
	  //       newWin.print();
	  //       newWin.close();	


	       html2canvas(document.querySelector("#capture")).then(canvas => {
			    document.body.appendChild(canvas)
			});
		};



		$scope.FamiliaSeleccionada = function(familia){
			$scope.famslected = JSON.parse(familia);
			$scope.codigo.color = '';
			$scope.listatallas = '';

			var tallashombre = ['28','30','32','34','36','38','40','42','44','46','48','50','52','VAR'];
			var tallasmujer = ['3','5','7','9','11','13','15','17','19','21','23','25','27','VAR'];
			var tallasvarias = ['XS','S','M','G','XL','2XL','3XL','4XL','5XL','6XL','7XL','8XL','9XL','VAR'];
			if($scope.famslected.tipotalla=='TALLAS HOMBRE')
				$scope.listatallas = tallashombre;	
			if($scope.famslected.tipotalla=='TALLAS MUJER')
				$scope.listatallas = tallasmujer;	
			if($scope.famslected.tipotalla=='TALLAS VARIAS')
				$scope.listatallas = tallasvarias;	
		};


		// Función para generar los códigos de barras
	    $scope.BtnVerCodigos = function(codigo){
	        if(codigo == undefined || codigo.familia == undefined || codigo.familia == null || codigo.familia == ''){
	            toaster.pop('warning','AVISO','Debes elegir una familia para poder continuar.');
	        }
	        else if(codigo.color == undefined || codigo.color == null || codigo.color == ''){
	            toaster.pop('warning','AVISO','Debes elegir un color para poder continuar.');
	        }
	        else{
	            var tallaslista = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N'];
	            var codigofamiliascan = JSON.parse(codigo.familia);
				var codigocolorscan = JSON.parse(codigo.color);
	            $scope.ListaCodigosBarras = [];
	            var modelocodigosbarras = '';
	            $scope.tallaaguardar = '';

	            angular.forEach(tallaslista, function(valuetalla, key) {
	            	if(valuetalla=='A' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=28;
	            	if(valuetalla=='B' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=30;
	            	if(valuetalla=='C' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=32;
	            	if(valuetalla=='D' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=34;
	            	if(valuetalla=='E' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=36;
	            	if(valuetalla=='F' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=38;
	            	if(valuetalla=='G' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=40;
	            	if(valuetalla=='H' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=42;
	            	if(valuetalla=='I' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=44;
	            	if(valuetalla=='J' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=46;
	            	if(valuetalla=='K' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=48;
	            	if(valuetalla=='L' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=50;
	            	if(valuetalla=='M' && codigofamiliascan.tipotalla=='TALLAS HOMBRE')
	            		$scope.tallaaguardar=52;



	            	if(valuetalla=='A' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=3;
	            	if(valuetalla=='B' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=5;
	            	if(valuetalla=='C' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=7;
	            	if(valuetalla=='D' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=9;
	            	if(valuetalla=='E' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=11;
	            	if(valuetalla=='F' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=13;
	            	if(valuetalla=='G' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=15;
	            	if(valuetalla=='H' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=17;
	            	if(valuetalla=='I' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=19;
	            	if(valuetalla=='J' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=21;
	            	if(valuetalla=='K' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=23;
	            	if(valuetalla=='L' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=25;
	            	if(valuetalla=='M' && codigofamiliascan.tipotalla=='TALLAS MUJER')
	            		$scope.tallaaguardar=27;


	            	if(valuetalla=='A' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='XS';
	            	if(valuetalla=='B' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='S';
	            	if(valuetalla=='C' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='M';
	            	if(valuetalla=='D' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='L';
	            	if(valuetalla=='E' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='XL';
	            	if(valuetalla=='F' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='2XL';
	            	if(valuetalla=='G' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='3XL';
	            	if(valuetalla=='H' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='4XL';
	            	if(valuetalla=='I' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='5XL';
	            	if(valuetalla=='J' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='6XL';
	            	if(valuetalla=='K' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='7XL';
	            	if(valuetalla=='L' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='8XL';
	            	if(valuetalla=='M' && codigofamiliascan.tipotalla=='TALLAS VARIAS')
	            		$scope.tallaaguardar='9XL';

	            	if(valuetalla=='N')
	            		$scope.tallaaguardar='VAR';


                    modelocodigosbarras = { codigobarras : codigofamiliascan.folio + codigocolorscan.folio + valuetalla, 
                    						familia:codigofamiliascan.familia, 
                    						color:codigocolorscan.color, 
                    						talla:$scope.tallaaguardar,
                    						tipotalla : codigofamiliascan.tipotalla};
                    $scope.ListaCodigosBarras.push(modelocodigosbarras);    
                });


	        }
	    };


	    // Actualiza las cantidades de salida cuando se hace manual mente
	    $scope.ActualizaSalidaCantidad = function(corte,tallas,index){
	    	if(tallas.cantidadsalida*1>tallas.cantidad){
	    		tallas.cantidadsalida = 0;
	    		toaster.pop('warning','AVISO','La cantidad a sacar no puede ser mayor a la existencia.');
	    		// $scope.ListaSalidas[index].
	    	}
	    	else{
	    		ActualizaTotalCorteSalida(corte);
	    		ActualizaTotaldeTotalesSalidas();
	    	}
	    };


	    // Función para encontrar el último día del mes para el inventario final
	    var lastday = function(y,m){
			return  new Date(y, m +1, 0).getDate();
		}
		console.log(lastday(2014,0));


		// Función para exportar a excel con un factory
	    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
	        var exportHref = Excel.tableToExcel(tableId, 'Hoja 1');
	        var a = document.createElement('a');
	        a.href = exportHref;
	        a.download = 'Inventario.xls';
	        a.click();
	    };


	     // Función para exportar a PDF
	    $scope.BtnExportPDF = function () {
	        html2canvas(document.getElementById("capture"),
	        {
	            width: 1800,
	            height: 1000,
	            //allowTaint: true,
	            //foreignObjectRendering: true
	        }).then(canvas => {
	            var dataURL = canvas.toDataURL();
	            //document.body.appendChild(canvas)
	            console.log(dataURL);
	            var docDefinition = {
	                content: [{
	                    image: dataURL,
	                    width:720,
	                }]
	            };
	            pdfMake.createPdf(docDefinition).download("Reporte_Carga_Rutas.pdf");
	        });
	    };



	    $scope.AgregaCorteSalidaManual = function(codigomanual){
	    	var codigoarmadomanual = '';
	    	var tallamanual = '';
	    	var familiatemp = JSON.parse(codigomanual.familia);
	    	var colortemp = JSON.parse(codigomanual.color);

	    	if(codigomanual.tallalista=='3' || codigomanual.tallalista=='28' || codigomanual.tallalista=='XS')
	    		tallamanual='A';	
	    	if(codigomanual.tallalista=='5' || codigomanual.tallalista=='30' || codigomanual.tallalista=='S')
	    		tallamanual='B';
	    	if(codigomanual.tallalista=='7' || codigomanual.tallalista=='32' || codigomanual.tallalista=='M')
	    		tallamanual='C';
	    	if(codigomanual.tallalista=='9' || codigomanual.tallalista=='34' || codigomanual.tallalista=='G')
	    		tallamanual='D';
	    	if(codigomanual.tallalista=='11' || codigomanual.tallalista=='36' || codigomanual.tallalista=='XL')
	    		tallamanual='E';
	    	if(codigomanual.tallalista=='13' || codigomanual.tallalista=='38' || codigomanual.tallalista=='2XL')
	    		tallamanual='F';
	    	if(codigomanual.tallalista=='15' || codigomanual.tallalista=='40' || codigomanual.tallalista=='3XL')
	    		tallamanual='G';
	    	if(codigomanual.tallalista=='17' || codigomanual.tallalista=='42' || codigomanual.tallalista=='4XL')
	    		tallamanual='H';
	    	if(codigomanual.tallalista=='19' || codigomanual.tallalista=='44' || codigomanual.tallalista=='5XL')
	    		tallamanual='I';
	    	if(codigomanual.tallalista=='21' || codigomanual.tallalista=='46' || codigomanual.tallalista=='6XL')
	    		tallamanual='J';
	    	if(codigomanual.tallalista=='23' || codigomanual.tallalista=='48' || codigomanual.tallalista=='7XL')
	    		tallamanual='K';
	    	if(codigomanual.tallalista=='25' || codigomanual.tallalista=='50' || codigomanual.tallalista=='8XL')
	    		tallamanual='L';
	    	if(codigomanual.tallalista=='27' || codigomanual.tallalista=='52' || codigomanual.tallalista=='9XL')
	    		tallamanual='M';
	    	if(codigomanual.tallalista=='VAR')
	    		tallamanual='N';


	    	codigoarmadomanual = familiatemp.folio + colortemp.folio + tallamanual;


	    	$scope.CodigoEscaneado(codigoarmadomanual);

	    	$scope.codigo.familia = '';
	    	$scope.codigo.color = '';
	    	$scope.codigo.tallalista = '';
	    };


	}]);